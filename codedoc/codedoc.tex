\documentclass[11pt,a4paper]{article}

\usepackage[top=2.0cm, bottom=2.0cm, left=1.8cm, right=1.8cm]{geometry}
\usepackage{xspace,amsmath,amssymb}

\usepackage[colorlinks=true]{hyperref}
\usepackage[titles]{tocloft}
\renewcommand{\cftdot}{}

\parindent0pt
\linespread{1.2}
%\pagestyle{empty}

\newcommand{\mrm}[1]{\mathrm{#1}}
\newcommand{\mcl}[1]{\mathcal{#1}}
\newcommand{\ttt}[1]{\texttt{#1}}
\newcommand{\tbf}[1]{\textbf{#1}}
\newcommand{\tsc}[1]{\textsc{#1}}
\newcommand{\tul}[1]{\underline{#1}}
\renewcommand{\t}[1]{\text{#1}}

\newcommand{\py}{Pythia\,8\xspace}
\newcommand{\dire}{Dire\xspace}
\newcommand{\rivet}{Rivet\xspace}
\newcommand{\prof}{Professor\xspace}
\newcommand{\hpmc}{HepMC\xspace}
\newcommand{\fj}{fastjet\xspace}
\newcommand{\lhapdf}{LHAPDF\xspace}
\newcommand{\yoda}{yoda\xspace}
\newcommand{\jup}{Jupyter\xspace}
\newcommand{\pyt}{Python\xspace}

\newcommand{\df}{\ttt{Dockerfile}\xspace}
\newcommand{\rs}{\ttt{makeContainer.sh}\xspace}

\newcommand{\pcv}{\ttt{hepstore/professor:2.2.2}\xspace}
\newcommand{\rcv}{\ttt{hepstore/rivet:2.5.4}\xspace}
\newcommand{\profv}{\ttt{Professor-2.2.2}\xspace}
\newcommand{\fld}{\ttt{PythiaDireContainer/basisContainer/rivetBasis}\xspace}
\newcommand{\flc}{\ttt{rivetbasis}\xspace}

\newcommand{\lhapdfv}{\ttt{LHAPDF-6.2.1}\xspace}
\newcommand{\pyv}{\ttt{pythia8235}\xspace}
\newcommand{\direv}{\ttt{DIRE-2.002}\xspace}
\newcommand{\pyex}{\ttt{pythiaHepMC.cc}\xspace}
\newcommand{\direex}{\ttt{dire-vis.cc}\xspace}
\newcommand{\sld}{\ttt{PythiaDireContainer/basisContainer}\xspace}
\newcommand{\slc}{\ttt{pythiadirebasis}\xspace}

\newcommand{\tld}{\ttt{PythiaDireContainer}\xspace}
\newcommand{\tlc}{\ttt{pythiadire}\xspace}

\newcommand{\pdc}{\ttt{pythiatools/pythiadire}\xspace}

\begin{document}

\begin{center}
  \tbf{\LARGE{Docker Container for \py and \dire}}
\end{center}
\vspace*{2mm}

{\hypersetup{linkcolor=black}
\tableofcontents
}

\newpage

\section{Structure of the Container}

The docker container for \py and \dire is made up of three layers.

\subsection{First Layer: \rivet and \prof}

The first layer inherits from a \prof container; currently \pcv which is build
upon \rcv). It includes \rivet as well as \prof and \rivet dependencies.
\par\medskip
The \df, the script \rs to build the container and all other required files
are located in the subdirectory \fld. The resulting container is called \flc.
\par\medskip
A couple of modifications to the \prof container are performed to obtain the
expected behaviour:
\begin{itemize}
\item Installation of \ttt{poppler-utils} to use \ttt{pdfunite} to build
booklets of \rivet plots.
\item Installation of \ttt{rsync} as required to install \py and \dire.
\item Modification of \ttt{rivet} to allow use with self-written \hpmc
output.
\item Modification of \ttt{rivet-mkhtml} to allow use of \ttt{pdfunite} to
build booklets of \rivet plots.
\item Ugly hack to obtain a working version of \t{dot} as required by \dire to
produce 2D visualization of events. Since I have been unable to install
\ttt{dot} properly on the \prof container, some files are copied to the
container: \ttt{libgvc.so.6.0.0}, \ttt{libcgraph.so.6.0.0},
\ttt{libcdt.so.5.0.0}, \ttt{libpathplan.so.4.0.0}, \ttt{graphviz.tar},
\ttt{dot}.
\item A hacked version of \prof (\profv) is used to update the container, as
currently the available versions of \prof do either not include eigentunes or
the interpolation does not write out any results.
\end{itemize}

\subsection{Second Layer: \py and \dire}

The second layer inherits form the first layer container \flc.
\par\medskip
The \df, the script \rs to build the container and all other required files
are located in the subdirectory \sld. The resulting container is called \slc.
\par\medskip
The following modifications are performed:
\begin{itemize}
\item Installation of \jup, as well as \jup and \pyt extensions.
\item Installation of \lhapdfv.
\item Installation of \pyv, configured with \pyt, \hpmc, \fj, and \lhapdf. \\
NOTE: Depending on memory availability, this might freeze your computer for
a short while.
\item Installation of \direv, configured with \py. \\
NOTE: This is the released version of \dire, which does not include the 3D
visualization of events. These files have been added by hand.
\item Installation of \pyex, which is later used in the \jup notebooks.
\item Installation of a modified version of \direex, as \ttt{C++ cin} does not
work properly with the container.
\item Update environment variables \ttt{PYTHONPATH} and
\ttt{LD\_LIBRARY\_PATH}.
\end{itemize}

\subsection{Third Layer: Scripts and \jup Notebooks}

The third layer inherits form the second layer container \slc.
\par\medskip
The \df, the script \rs to build the container and all other required files
are located in the subdirectory \tld. The resulting container is called \tlc.
\par\medskip
The following modifications are performed:
\begin{itemize}
\item Add \pyt files (\ttt{*.py}) and update environment variable
\ttt{PYTHONPATH}.
\item Add \jup notebooks (\ttt{*.ipynb}) and \ttt{README}.
\item Add executables \ttt{jupyterNotebooks} to run \jup notebooks,
\ttt{eventGeneratorRun} to run event generation, and \ttt{tuning} to
produce tuning scripts.
\item Add executables \ttt{yoda-envelopes} to build envelopes of \yoda files 
and \ttt{rivet-mkhtml-custom} to run rivet-mkhtml with custom Rivet analyses
on the host system.
\item Add template scripts \ttt{runMC.sh}, \ttt{runIpol.sh}, \ttt{runTune.sh},
and \ttt{runBestTune.sh}.
\end{itemize}

\section{Pushing to Docker Hub}

To push a new or update an existing version of the container, do
\begin{verbatim}
     docker login
     docker tag pythiadire pythiatools/pythiadire:X.Y.Z
     docker push pythiatools/pythiadire:X.Y.Z
\end{verbatim}
where you login with your docker hub username. \ttt{pythiatools} is a group,
to which new members can be added upon request.

\section{Documentation}

A documentation is available at
\begin{verbatim}
     dire.gitlab.io/Docker.html
\end{verbatim}
and should be updated if new versions become available.

\section{Notes on the \pyt Files}

\subsection{\ttt{plotting.py}}

Class \ttt{PDF} to display PDFs in \jup cells.
\par\medskip
Class \ttt{RUNRIVET} to display a choice of \rivet analyses upon construction,
to start a \rivet run via the method \ttt{start\_rivet}, and to produce plots
via the method \ttt{produce\_plots}.
\par\medskip
Class \ttt{MULTHIST} to display a choice of multiplicity histograms, to
analyze an event and fill the histograms via the method \ttt{analyze\_event},
and to print the plots to file via the method \ttt{print\_plots}.

\subsection{\ttt{py8settings.py}}

Contains several functions to display \py and \dire settings and to allow the
user to write the chosen settings to a file or directly apply them through
\ttt{pythia.readString}. The processes are extracted in the function
\ttt{find\_procs()} via a walkthrough of the files in the \py directory
\ttt{xmldoc}.

\subsection{\ttt{hepmcdummy.py}}

Pseudo interface to \hpmc. Produces a text file or fifo with stripped \hpmc
events (similar to the result of \hpmc zipper): one vertex only, with incoming
and outgoing, but no intermediate particles. The main function, to be called
from a \jup notebook, is called
\ttt{event\_hepmc(filename, iEvent, nEvent, pythia, fifo)}, where the arguments
are the name of the text file or fifo, the number of the current event, the
total number of events, the \py object, and whether or not to use a fifo.

\section{Notes for Updates}

\begin{itemize}
\item The script \ttt{tuning} contains the name of the container. For a new
version, the script has to be updated.
\item Updating to new \py or \dire versions should be straight forward by
modifying \df in \sld. Complications might arise from changes in interfaces
or major changes in the code structure. \\
In \ttt{py8settings.py}, the default values for the shower and fragmentation
parameters for \py and \dire are hardcoded. If these values change, the file
has to be updated.
\item Updating to new \prof or \rivet versions should also be straight forward
by modifying \df in \fld to inherit from a new \prof or \rivet container.
\end{itemize}

The following table gives and overview of the files that are added to or
modified in the container and their dependencies (i.e. the software and scripts
they are using):
\par\medskip
\begin{tabular}{lll}
File & Dependencies & Added/Modified \\ \hline
\ttt{rivet} & \rivet & Modified in \flc. \\
\ttt{rivet-mkhtml} & \rivet & Modified in \flc. \\
\ttt{pythiaHepMC.cc} & \py & Added to \slc. \\
\ttt{dire-vis.cc} & \dire (+ \py) & Added to \slc. \\
\ttt{plotting.py} & \rivet, \py event and particle format & Added to \tlc. \\
\ttt{py8settings.py} & \py, \dire & Added to \tlc. \\
\ttt{hepmcdummy.py} & \hpmc file format, \py event and & Added to \tlc. \\
                    & particle format & \\
%%%%%%%%%%%%%
\ttt{pythiaPI.ipynb} & \py, \ttt{plotting.py}, \ttt{py8settings.py} & Added to \tlc. \\
\ttt{pythiaRivetPI.ipynb} & \py, \rivet, \ttt{hepmcdummy.py}, & Added to \tlc. \\
                          & \ttt{plotting.py}, \ttt{py8settings.py} & \\
\ttt{pythiaRivet.ipynb} & \py, \rivet, \ttt{plotting.py}, & Added to \tlc. \\
                        & \ttt{py8settings.py} & \\
\ttt{pythiaRivetUS.ipynb} & \py, \rivet, \ttt{plotting.py} & Added to \tlc. \\
\ttt{direRivet.ipynb} & \dire (+ \py), \rivet, \ttt{plotting.py}, & Added to \tlc. \\
                      & \ttt{py8settings.py} & \\
\ttt{direRivetUS.ipynb} & \dire (+ \py), \rivet, \ttt{plotting.py} & Added to \tlc. \\
\ttt{direEvent.ipynb} & \dire (+ \py), \ttt{plotting.py}, & Added to \tlc. \\
                      & \ttt{py8settings.py} & \\
\ttt{tuning.ipynb} & \prof, \rivet, \py, \dire, & Added to \tlc. \\
                   & \pdc, \ttt{plotting.py}, & \\
                   & \ttt{eventGeneratorRun}, \ttt{yoda-envelopes} & \\
%%%%%%%%%%%%%
\ttt{README} & None & Added to \tlc. \\
\ttt{jupyterNotebooks} & \pdc, \jup & Added to \tlc. \\
\ttt{eventGeneratorRun} & \rivet, \py, \dire, \ttt{pythiaHepMC.cc} & Added to \tlc. \\
\ttt{tuning} & \pdc, \prof, & Added to \tlc. \\
             & \ttt{runMC.sh}, \ttt{runIpol.sh}, \ttt{runTune.sh}, & \\
             & \ttt{runBestTune.sh} & \\
\ttt{yoda-envelopes} & \yoda file format & Added to \tlc. \\
\ttt{rivet-mkhtml-custom} & \rivet & Added to \tlc. \\
\ttt{runMC.sh} & \pdc, & Added to \tlc. \\
               & \ttt{eventGeneratorRun} & \\
\ttt{runIpol.sh} & \pdc, \prof & Added to \tlc. \\
\ttt{runTune.sh} & \pdc, \prof & Added to \tlc. \\
\ttt{runBestTune.sh} & \pdc, \prof, & Added to \tlc. \\
                     & \ttt{eventGeneratorRun}, \ttt{yoda-envelopes}, & \\
                     & \ttt{rivet-mkhtml-custom} &
\end{tabular}

\end{document}
