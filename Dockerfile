FROM pythiabasis

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y vim
RUN apt-get install -y bash-completion
RUN apt-get install -y pdftk-java

# note: if you don't worry about size, do this
#RUN apt-get install -y texlive-full

# else, do this
RUN apt-get install -y adwaita-icon-theme aglfn asymptote at-spi2-core biber chktex \
  cm-super cm-super-minimal context context-modules dbus-user-session \
  dconf-gsettings-backend dconf-service dmsetup dvidvi dvipng emacsen-common \
  feynmf fragmaster freeglut3 gir1.2-glib-2.0 glib-networking glib-networking-common \
  glib-networking-services gsettings-desktop-schemas gtk-update-icon-cache \
  humanity-icon-theme info install-info javascript-common lacheck \
  latexdiff latexmk \
  lcdf-typetools libalgorithm-c3-perl libapache-pom-java libargon2-1 \
  libatk-bridge2.0-0 libatk1.0-0 libatk1.0-data libatspi2.0-0 \
  libautovivification-perl libb-hooks-endofscope-perl libb-hooks-op-check-perl \
  libbtparse2 libbusiness-isbn-data-perl libbusiness-isbn-perl \
  libbusiness-ismn-perl libbusiness-issn-perl libcairo-gobject2 libcap2 \
  libclass-accessor-perl libclass-c3-perl libclass-c3-xs-perl \
  libclass-data-inheritable-perl libclass-inspector-perl \
  libclass-method-modifiers-perl libclass-singleton-perl \
  libclass-xsaccessor-perl libclone-perl libcolord2 libcommons-logging-java \
  libcommons-parent-java libcryptsetup12 libdata-compare-perl \
  libdata-optlist-perl libdata-uniqid-perl libdate-simple-perl \
  libdatetime-calendar-julian-perl libdatetime-format-builder-perl \
  libdatetime-format-strptime-perl libdatetime-locale-perl libdatetime-perl \
  libdatetime-timezone-perl libdconf1 libdevel-callchecker-perl \
  libdevel-caller-perl libdevel-globaldestruction-perl libdevel-lexalias-perl \
  libdevel-stacktrace-perl libdevmapper1.02.1 libdist-checkconflicts-perl \
  libdouble-conversion3 libdynaloader-functions-perl libegl-mesa0 libegl1 \
  libemail-date-format-perl libemf1 libencode-eucjpms-perl \
  libencode-hanextra-perl libencode-jis2k-perl libencode-perl libepoxy0 \
  libeval-closure-perl libevdev2 libexception-class-perl libexporter-tiny-perl \
  libfile-find-rule-perl libfile-homedir-perl libfile-sharedir-perl \
  libfile-slurper-perl libfile-which-perl libfontbox-java libgbm1 libgc1c2 \
  libgdk-pixbuf2.0-0 libgdk-pixbuf2.0-bin libgdk-pixbuf2.0-common \
  libgirara-gtk3-3 libgirepository-1.0-1 libglew2.1 libgtk-3-0 libgtk-3-bin \
  libgtk-3-common libgudev-1.0-0 libinput-bin libinput10 libip4tc2 \
  libipc-run3-perl libipc-shareable-perl libjs-jquery libjson-c4 \
  libjson-glib-1.0-0 libjson-glib-1.0-common libkmod2 liblingua-translit-perl \
  liblist-allutils-perl liblist-moreutils-perl liblist-someutils-perl \
  liblist-someutils-xs-perl liblist-utilsby-perl liblog-dispatch-perl \
  liblog-log4perl-perl libmagick++-6.q16-8 libmail-sendmail-perl \
  libmime-charset-perl libmime-lite-perl libmime-types-perl \
  libmodule-implementation-perl libmodule-runtime-perl libmro-compat-perl \
  libmtdev1 libnamespace-autoclean-perl libnamespace-clean-perl libnss-systemd \
  libnumber-compare-perl libosp5 libostyle1c2 libpackage-stash-perl \
  libpackage-stash-xs-perl libpadwalker-perl libpam-systemd \
  libparams-classify-perl libparams-util-perl libparams-validate-perl \
  libparams-validationcompiler-perl libparse-recdescent-perl libpcre2-16-0 \
  libpdfbox-java libperlio-utf8-strict-perl libplot2c2 libpoppler-glib8 \
  libpoppler-qt5-1 libproxy1v5 libpstoedit0c2a libqt5core5a libqt5dbus5 \
  libqt5gui5 libqt5network5 libqt5svg5 libqt5widgets5 libqt5xml5 \
  libreadonly-perl libref-util-perl libref-util-xs-perl libregexp-common-perl \
  librest-0.7-0 librole-tiny-perl librsvg2-2 librsvg2-common libruby2.7 \
  libsombok3 libsort-key-perl libsoup-gnome2.4-1 libsoup2.4-1 libspecio-perl \
  libsub-exporter-perl libsub-exporter-progressive-perl libsub-identify-perl \
  libsub-install-perl libsub-name-perl libsub-quote-perl \
  libsys-hostname-long-perl libtcl8.6 libtext-bibtex-perl libtext-csv-perl \
  libtext-csv-xs-perl libtext-glob-perl libtext-roman-perl \
  libtext-unidecode-perl libtie-cycle-perl libtk8.6 libunicode-linebreak-perl \
  libutempter0 libvariable-magic-perl libwacom-bin libwacom-common libwacom2 \
  libwayland-cursor0 libwayland-egl1 libwayland-server0 libxcb-icccm4 \
  libxcb-image0 libxcb-keysyms1 libxcb-render-util0 libxcb-util1 \
  libxcb-xfixes0 libxcb-xinerama0 libxcb-xinput0 libxcb-xkb1 \
  libxkbcommon-x11-0 libxkbcommon0 libxml-libxml-perl \
  libxml-libxml-simple-perl libxml-libxslt-perl libxml-namespacesupport-perl \
  libxml-sax-base-perl libxml-sax-expat-perl libxml-sax-perl \
  libxml-writer-perl libxslt1.1 libxss1 libxstring-perl libyaml-0-2 \
  libyaml-tiny-perl libzip5 networkd-dispatcher openjade pfb2t1c2pfb prerex \
  preview-latex-style ps2eps pstoedit psutils purifyeps python3-dbus \
  python3-gi qt5-gtk-platformtheme qttranslations5-l10n rake ruby \
  ruby-minitest ruby-net-telnet ruby-power-assert ruby-test-unit ruby-xmlrpc \
  ruby2.7 rubygems-integration sgml-base systemd systemd-sysv \
  systemd-timesyncd tcl tcl8.6 teckit texinfo texlive-bibtex-extra \
  texlive-extra-utils texlive-font-utils texlive-formats-extra texlive-latex-extra \
  texlive-luatex texlive-metapost texlive-pictures \
  texlive-plain-generic texlive-pstricks \
  texlive-publishers texlive-science texlive-xetex tk tk8.6 ubuntu-mono unzip \
  vprerex xbitmaps xkb-data xterm zathura zathura-pdf-poppler zip

RUN cd /etc/ImageMagick-6/ \
    && sed -i 's,domain="coder" rights="none",domain="coder" rights="read | write",g' /etc/ImageMagick-6/policy.xml
#
## modifications in Rivet scripts
#RUN cp /usr/local/bin/rivet-mkhtml /usr/local/bin/rivet-mkhtml.bak
#RUN sed -i 's/pdftk or pdfmerge/pdftk, pdfmerge, or pdfunite/g' /usr/local/bin/rivet-mkhtml
#RUN sed -i 's/pdftk nor pdfmerge/pdftk, pdfmerge, nor pdfunite/g' /usr/local/bin/rivet-mkhtml
#RUN sed -i '/elif which("pdfmerge") is not None:/i\ \ \ \ \ \ \ \ \ \ \ \ elif which("pdfunite") is not None:\n\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ bookletcmd = ["pdfunite"]\n\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ for analysis in analyses:\n\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ anapath = os.path.join(opts.OUTPUTDIR, analysis)\n\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ bookletcmd += sorted(glob.glob("%s\/*.pdf" % anapath))\n\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ bookletcmd += ["%s\/booklet.pdf" % opts.OUTPUTDIR]\n\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ print (bookletcmd)\n\ \ \ \ \ \ \ \ \ \ \ \ \ \ \ \ Popen(bookletcmd).wait()' /usr/local/bin/rivet-mkhtml

# add python files and set environment variable
ADD plotting.py /usr/local/src
ADD py8settings.py /usr/local/src
ADD hepmcdummy.py /usr/local/src
ENV PYTHONPATH "$PYTHONPATH:/usr/local/src"

# add notebooks
RUN mkdir /usr/local/share/notebooks
ADD README /usr/local/share/notebooks
ADD pythiaPI.ipynb /usr/local/share/notebooks
ADD pythiaEvent.ipynb /usr/local/share/notebooks
ADD pythiaRivet.ipynb /usr/local/share/notebooks

# add executable
ADD jupyterNotebooks /usr/local/bin
RUN chmod +x /usr/local/bin/jupyterNotebooks

CMD /usr/local/bin/jupyterNotebooks
